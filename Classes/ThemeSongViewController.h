/* ThemeSong
 * Copyright (C) 2010-2013  Mark Lown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
@class BasicSettingViewController;
@class SettingsViewController;

typedef enum{
	utModeOnOff,
	utModeVolume,
	utModeSelective
} utMode;

@interface ThemeSongViewController : UIViewController <MPMediaPickerControllerDelegate, UIAccelerometerDelegate>{

	BasicSettingViewController *basicSettingsViewController;
	SettingsViewController *settingsViewController;
	MPMusicPlayerController *player;
	MPMediaPickerController *picker;
	MPMediaItemCollection *theme;
	UIAccelerometer *accelerometer;
	IBOutlet UIButton *btnThemez;
	IBOutlet UIButton *btnPlay;
	
	// Member vars
	float px,py,pz;
	float xx,yy,zz;
	BOOL first;
	BOOL shouldPlay;
	BOOL btnShowsPlay;
	BOOL run;
	BOOL firstVol;
	BOOL songSelected;
	
	// Local counters
	int on;
	int off;
	int cnt;
	
	/* Basic properties */
	float threshold;
	utMode mode;
	float volumeMin;
	float volumeMax;
	
	/* Advanced properties */
	int cntToChangeState;
	float playLoopUpdateRate;
	float accLoopUpdateRate;
	float volumeScaleFactor;
	
	/* Default settings */
	float defThreshold;
	utMode defMode;
	float defVolumeMin;
	float defVolumeMax;
	int defCntToChangeState;
	float defPlayLoopUpdateRate;
	float defAccLoopUpdateRate;
	float defVolumeScaleFactor;
}

@property (nonatomic, retain) MPMusicPlayerController *player;
@property (nonatomic, retain) MPMediaPickerController *picker;
@property (nonatomic, retain) MPMediaItemCollection *theme;
@property (nonatomic, retain) UIAccelerometer *accelerometer;
@property (nonatomic, retain) BasicSettingViewController *basicSettingsViewController;
@property (nonatomic, retain) SettingsViewController *settingsViewController;

/* Basic properties that can be edited by the user to 
 * change the behavior of the app */
@property (nonatomic, assign) utMode mode;
@property (nonatomic, assign) float volumeMin;
@property (nonatomic, assign) float volumeMax;
@property (nonatomic, assign) BOOL oppositeDay;

/* Advanced properties that can be edited by the user to
 * change the behavior of the app - fine tuning */
@property (nonatomic, assign) float playLoopUpdateRate; // The update rate for the play loop
@property (nonatomic, assign) float accLoopUpdateRate;	// The update rate for the accelerometer
@property (nonatomic, assign) int cntToChangeState;		// The number of data points to compare to determine the state
@property (nonatomic, assign) float threshold;			// Threshold for dot product
@property (nonatomic, assign) float volumeScaleFactor;

/* Default settings properties 
 */
@property (nonatomic, assign) float defThreshold;
@property (nonatomic, assign) utMode defMode;
@property (nonatomic, assign) float defVolumeMin;
@property (nonatomic, assign) float defVolumeMax;
@property (nonatomic, assign) int defCntToChangeState;
@property (nonatomic, assign) float defPlayLoopUpdateRate;
@property (nonatomic, assign) float defAccLoopUpdateRate;
@property (nonatomic, assign) float defVolumeScaleFactor;
@property (nonatomic, assign) BOOL defOppositeDay;

-(IBAction)btnThemezPressed:(id)sender;
-(void)playInBackground;
-(void)settingsPressed;

@end

