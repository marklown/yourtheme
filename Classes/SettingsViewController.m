/* ThemeSong
 * Copyright (C) 2010-2013  Mark Lown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#import "SettingsViewController.h"
#import "ThemeSongAppDelegate.h"
#import "ThemeSongViewController.h"

@implementation SettingsViewController
@synthesize themeController, txtArray;

#pragma mark -
#pragma mark View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
	
	appDelegate = [[UIApplication sharedApplication] delegate];
	[appDelegate.rootController setNavigationBarHidden:NO animated:YES];
	self.themeController = appDelegate.themeController;
	
	// Initialize txtArray
	//txtArray = [NSArray	arrayWithObjects:
	//			@"Play loop update rate is the rate at which the software checks
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
	if(section == 0)
		return @"Basic settings";
	else if (section == 1)
		return @"Advanced settings";
	else {
		return @"Presets";
	}

}


/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/
/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

/* Handle slider actions */
-(void)sliderAction:(UISlider *)sender{
	if(sender == sliderMinVolume){
		themeController.volumeMin = sliderMinVolume.value;
	}else if(sender == sliderMaxVolume){
		themeController.volumeMax = sliderMaxVolume.value;
	}else if(sender == sliderPlayLoopUpdateRate){
		lblPlayLoopUpdateRate.text = [NSString stringWithFormat:@"%.2fs",sliderPlayLoopUpdateRate.value];
		themeController.playLoopUpdateRate = sliderPlayLoopUpdateRate.value;
	}else if(sender == sliderAccLoopUpdateRate){
		lblAccLoopUpdateRate.text = [NSString stringWithFormat:@"%.2fs",sliderAccLoopUpdateRate.value];
		themeController.accLoopUpdateRate = sliderAccLoopUpdateRate.value;
	}else if(sender == sliderCountToChangeState){
		lblCountToChangeState.text = [NSString stringWithFormat:@"%d",(int)sliderCountToChangeState.value];
		themeController.cntToChangeState = (int)sliderCountToChangeState.value;
		NSLog(@"%d",themeController.cntToChangeState);
	}else if(sender == sliderDotProductThreshold){
		lblDotProductThreshold.text = [NSString stringWithFormat:@"%.3f",sliderDotProductThreshold.value];
		themeController.threshold = sliderDotProductThreshold.value;
	}else if(sender == sliderVolumeControlFactor){
		lblVolumeControlFactor.text = [NSString stringWithFormat:@"%.2f",sliderVolumeControlFactor.value];
		themeController.volumeScaleFactor = sliderVolumeControlFactor.value;
	}else{
		themeController.volumeMin = sliderMinVolume.value;
		themeController.volumeMax = sliderMaxVolume.value;
		lblPlayLoopUpdateRate.text = [NSString stringWithFormat:@"%.2fs",sliderPlayLoopUpdateRate.value];
		themeController.playLoopUpdateRate = sliderPlayLoopUpdateRate.value;
		lblAccLoopUpdateRate.text = [NSString stringWithFormat:@"%.2fs",sliderAccLoopUpdateRate.value];
		themeController.accLoopUpdateRate = sliderAccLoopUpdateRate.value;
		lblCountToChangeState.text = [NSString stringWithFormat:@"%d",(int)sliderCountToChangeState.value];
		themeController.cntToChangeState = (int)sliderCountToChangeState.value;
		lblDotProductThreshold.text = [NSString stringWithFormat:@"%.3f",sliderDotProductThreshold.value];
		themeController.threshold = sliderDotProductThreshold.value;
		lblVolumeControlFactor.text = [NSString stringWithFormat:@"%.2f",sliderVolumeControlFactor.value];
		themeController.volumeScaleFactor = sliderVolumeControlFactor.value;
	}
}

/* Handle switch actions */
-(void)switchAction:(UISwitch *)sender{
	if(sender == switchOppositeDay){
		if(sender.on){
			themeController.oppositeDay = YES;
		}else{
			themeController.oppositeDay = NO;
		}
	}
}

-(void)buttonAction:(UISegmentedControl *)sender{
	switch(sender.selectedSegmentIndex){
		case 0:
			themeController.mode = utModeOnOff;
			break;
		case 1:
			themeController.mode = utModeVolume;
			break;
		case 2:
			themeController.mode = utModeSelective;
			break;
	}
}


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 3;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
	switch(section){
		case 0:
			return 4;
			break;
		case 1:
			return 5;
			break;
		case 2:
			return 1;
			break;
	}
	return 0;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell;// = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		
		// Configure the cell
		switch(indexPath.section){
			case 0: // Basic settings
				switch(indexPath.row){
					case 0:
						cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:14.0f];
						cell.textLabel.text = @"Minimum volume";
						sliderMinVolume = [[UISlider alloc] initWithFrame:CGRectMake(170,0,135,50)];
						sliderMinVolume.minimumValue = 0;
						sliderMinVolume.maximumValue = 1;
						sliderMinVolume.value = themeController.volumeMin;
						[sliderMinVolume addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
						[cell addSubview:sliderMinVolume];
						break;
					case 1:
						cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:14.0f];
						cell.textLabel.text = @"Maximum volume";
						sliderMaxVolume = [[UISlider alloc] initWithFrame:CGRectMake(170,0,135,50)];
						sliderMaxVolume.minimumValue = 0;
						sliderMaxVolume.maximumValue = 1;
						sliderMaxVolume.value = themeController.volumeMax;
						[sliderMaxVolume addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
						[cell addSubview:sliderMaxVolume];
						break;
					case 2:
						cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:14.0f];
						cell.textLabel.text = @"Mode";
						modeControl = [[[UISegmentedControl alloc] initWithFrame:CGRectMake(85,5,217,35)] autorelease];
						[modeControl insertSegmentWithTitle:@"On/Off" atIndex:0 animated:NO];
						[modeControl insertSegmentWithTitle:@"Volume" atIndex:1 animated:NO];
						//[modeControl insertSegmentWithTitle:@"3 Song" atIndex:2 animated:NO] // don't add this for now, future version maybe
						[modeControl addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventValueChanged];
						modeControl.selectedSegmentIndex = themeController.mode;
						[cell addSubview:modeControl];
						break;
					case 3:
						cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:14.0f];
						cell.textLabel.text = @"Opposite day";
						switchOppositeDay = [[[UISwitch alloc] initWithFrame:CGRectZero] autorelease];
						[switchOppositeDay addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
						cell.accessoryView = switchOppositeDay;
						break;
				}
				break;
			case 1: // Advanced settings
				switch(indexPath.row){
					case 0:
						cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:13.0f];
						cell.textLabel.text = @"Play loop update rate:";
						sliderPlayLoopUpdateRate = [[[UISlider alloc] initWithFrame:CGRectMake(210,0,95,50)] autorelease];
						sliderPlayLoopUpdateRate.minimumValue = .100;
						sliderPlayLoopUpdateRate.maximumValue = .250;
						sliderPlayLoopUpdateRate.value = themeController.playLoopUpdateRate;
						[sliderPlayLoopUpdateRate addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
						lblPlayLoopUpdateRate = [[[UILabel alloc] initWithFrame:CGRectMake(168,8,40,30)] autorelease];
						lblPlayLoopUpdateRate.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
						lblPlayLoopUpdateRate.text = [NSString stringWithFormat:@"%.2fs",sliderPlayLoopUpdateRate.value];
						[cell addSubview:sliderPlayLoopUpdateRate];
						[cell addSubview:lblPlayLoopUpdateRate];
						break;
					case 1:
						cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:13.0f];
						cell.textLabel.text = @"Acc loop update rate:";
						sliderAccLoopUpdateRate = [[[UISlider alloc] initWithFrame:CGRectMake(210,0,95,50)] autorelease];
						sliderAccLoopUpdateRate.minimumValue = .100;
						sliderAccLoopUpdateRate.maximumValue = .250;
						sliderAccLoopUpdateRate.value = themeController.accLoopUpdateRate;
						[sliderAccLoopUpdateRate addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
						lblAccLoopUpdateRate = [[[UILabel alloc] initWithFrame:CGRectMake(168,7,40,30)] autorelease];
						lblAccLoopUpdateRate.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
						lblAccLoopUpdateRate.text = [NSString stringWithFormat:@"%.2fs",sliderAccLoopUpdateRate.value];
						[cell addSubview:lblAccLoopUpdateRate];
						[cell addSubview:sliderAccLoopUpdateRate];
						break;
					case 2:
						cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:13.0f];
						cell.textLabel.text = @"Dot product threshold:";
						sliderDotProductThreshold = [[[UISlider alloc] initWithFrame:CGRectMake(210,0,95,50)] autorelease];
						sliderDotProductThreshold.minimumValue = .900;
						sliderDotProductThreshold.maximumValue = .999;
						sliderDotProductThreshold.value = themeController.threshold;
						[sliderDotProductThreshold addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
						lblDotProductThreshold = [[[UILabel alloc] initWithFrame:CGRectMake(168,7,40,30)] autorelease];
						lblDotProductThreshold.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
						lblDotProductThreshold.text = [NSString stringWithFormat:@"%.3f",sliderDotProductThreshold.value];
						[cell addSubview:lblDotProductThreshold];
						[cell addSubview:sliderDotProductThreshold];
						break;
					case 3:
						cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:13.0f];
						cell.textLabel.text = @"Count to change state:";
						sliderCountToChangeState = [[[UISlider alloc] initWithFrame:CGRectMake(210,0,95,50)] autorelease];
						sliderCountToChangeState.minimumValue = 1;
						sliderCountToChangeState.maximumValue = 10;
						sliderCountToChangeState.value = themeController.cntToChangeState;
						[sliderCountToChangeState addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
						lblCountToChangeState = [[[UILabel alloc] initWithFrame:CGRectMake(168,7,40,30)] autorelease];
						lblCountToChangeState.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
						lblCountToChangeState.text = [NSString stringWithFormat:@"%d",(int)sliderCountToChangeState.value];
						[cell addSubview:lblCountToChangeState];
						[cell addSubview:sliderCountToChangeState];
						break;
					case 4:
						cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:13.0f];
						cell.textLabel.text = @"Volume control factor:";
						sliderVolumeControlFactor = [[[UISlider alloc] initWithFrame:CGRectMake(210,0,95,50)] autorelease];
						sliderVolumeControlFactor.minimumValue = .5;
						sliderVolumeControlFactor.maximumValue = 2;
						sliderVolumeControlFactor.value = themeController.volumeScaleFactor;
						[sliderVolumeControlFactor addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
						lblVolumeControlFactor = [[[UILabel alloc] initWithFrame:CGRectMake(168,7,40,30)] autorelease];
						lblVolumeControlFactor.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
						lblVolumeControlFactor.text = [NSString stringWithFormat:@"%.2f",sliderVolumeControlFactor.value];
						[cell addSubview:lblVolumeControlFactor];
						[cell addSubview:sliderVolumeControlFactor];
						break;
					default:
						break;
				}
				break;
			case 2: // Presets
				switch(indexPath.row){
					case 0:
						cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:14.0f];
						cell.textLabel.text = @"Default";
						break;
				}
				break;
		}
    //}
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if(indexPath.section == 2){
		switch(indexPath.row){
			case 0: // Defaults
				themeController.volumeMax = themeController.defVolumeMax;
				themeController.volumeMin = themeController.defVolumeMin;
				themeController.mode = themeController.defMode;
				themeController.playLoopUpdateRate = themeController.defPlayLoopUpdateRate;
				themeController.accLoopUpdateRate = themeController.defAccLoopUpdateRate;
				themeController.threshold = themeController.defThreshold;
				themeController.cntToChangeState = themeController.defCntToChangeState;
				themeController.volumeScaleFactor = themeController.defVolumeScaleFactor;
				themeController.oppositeDay = themeController.defOppositeDay;
				[myTableView reloadData];
				break;
		}
	}else{
		/* Future place to show a subview with information about what this setting does */
		
		/*
		UITextView *txtInfo = [[UITextView alloc] initWithFrame:CGRectMake(50,50,220,220)];
		txtInfo.backgroundColor = [UIColor grayColor];
		txtInfo.alpha = .9;
		txtInfo.text = @"info some info some info about this thingy";
		[self.view addSubview:txtInfo];
		 */
		
		
	}
	
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
	[txtArray release];
	[themeController release];
    [super dealloc];
}


@end

