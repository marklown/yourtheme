//
//  AdvancedSettingViewController.h
//  ThemeSong
//
//  Created by Mark Lown on 9/23/10.
//  Copyright 2010 PopCulture Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ThemeSongViewController;
@class ThemeSongAppDelegate;

@interface AdvancedSettingViewController : UIViewController {

	IBOutlet UITextField *txtPlayLoopUpdateRate;
	IBOutlet UITextField *txtAccLoopUpdateRate;
	IBOutlet UITextField *txtThreshold;
	IBOutlet UITextField *txtCount;
	
	ThemeSongViewController *themeController;
	ThemeSongAppDelegate *appDelegate;
}

@property (nonatomic, retain) ThemeSongViewController *themeController;
@property (nonatomic, retain) ThemeSongAppDelegate *appDelegate;

-(IBAction)txtUpdated:(id)sender;

@end
