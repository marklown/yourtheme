//
//  BasicSettingViewController.m
//  ThemeSong
//
//  Created by Mark Lown on 9/22/10.
//  Copyright 2010 PopCulture Development, LLC. All rights reserved.
//

#import "BasicSettingViewController.h"
#import "ThemeSongAppDelegate.h"
#import "ThemeSongViewController.h"
#import "AdvancedSettingViewController.h"

@implementation BasicSettingViewController
@synthesize themeController, advancedController, appDelegate;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	sliderMaxVolume.minimumValue = 0;
	sliderMaxVolume.maximumValue = 1;
	sliderMinVolume.minimumValue = 0;
	sliderMinVolume.maximumValue = 1;
	self.title = @"Basic settings";
	appDelegate = [[UIApplication sharedApplication] delegate];
	self.themeController = appDelegate.themeController;
}

- (void)viewWillAppear:(BOOL)animated{
	[appDelegate.rootController setNavigationBarHidden:NO animated:NO];
	
	[sliderMaxVolume setValue:themeController.volumeMax animated:NO];
	[sliderMinVolume setValue:themeController.volumeMin animated:NO];
}

-(IBAction)sliderValueChanged:(id)sender{
	UISlider *slider = (UISlider *)sender;
	if(slider == sliderMaxVolume){
		if(slider.value < sliderMinVolume.value){
			[slider setValue:sliderMinVolume.value animated:YES];
			//slider.value = sliderMinVolume.value;
			themeController.volumeMax = slider.value;
		}else{
			themeController.volumeMax = slider.value;
		}
	}else if(slider == sliderMinVolume){
		if(slider.value > sliderMaxVolume.value){
			[slider setValue:sliderMaxVolume.value animated:YES];
			//slider.value = sliderMaxVolume.value;
			themeController.volumeMax = slider.value;
		}else{
			themeController.volumeMin = slider.value;
		}
	}
}

-(IBAction)btnPressed:(id)sender{
	advancedController = [[AdvancedSettingViewController alloc] initWithNibName:@"AdvancedSettingViewController" bundle:nil];
	//advancedController.themeController = themeController;
	[appDelegate.rootController pushViewController:self.advancedController animated:YES];
}

-(IBAction)switchPressed:(id)sender{
	UISwitch *sw = (UISwitch *)sender;
	if(sw == switchOnOff){
		if(sw.on){
			[switchVolume setOn:NO animated:YES];
			[switchSelective setOn:NO animated:YES];
			themeController.mode = utModeOnOff;
		}else{
			[switchVolume setOn:YES animated:YES];
			[switchSelective setOn:NO animated:YES];
			themeController.mode = utModeVolume;
		}
	}else if(sw == switchVolume){
		if(sw.on){
			[switchOnOff setOn:NO animated:YES];
			[switchSelective setOn:NO animated:YES];
			themeController.mode = utModeVolume;
		}else{
			[switchOnOff setOn:YES animated:YES];
			[switchSelective setOn:NO animated:YES];
			themeController.mode = utModeOnOff;
		}
	}else if(sw == switchSelective){
		if(sw.on){
			[switchOnOff setOn:NO animated:YES];
			[switchVolume setOn:NO animated:YES];
			themeController.mode = utModeSelective;
		}else{
			[switchOnOff setOn:YES animated:YES];
			[switchVolume setOn:NO animated:YES];
			themeController.mode = utModeOnOff;
		}
	}
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[appDelegate release];
	[advancedController release];
	[themeController release];
    [super dealloc];
}


@end
