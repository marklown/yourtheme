//
//  BasicSettingViewController.h
//  ThemeSong
//
//  Created by Mark Lown on 9/22/10.
//  Copyright 2010 PopCulture Development, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ThemeSongViewController;
@class AdvancedSettingViewController;
@class ThemeSongAppDelegate;

@interface BasicSettingViewController : UIViewController {

	ThemeSongViewController *themeController;
	AdvancedSettingViewController *advancedController;
	
	IBOutlet UISlider *sliderMinVolume;
	IBOutlet UISlider *sliderMaxVolume;
	IBOutlet UISwitch *switchOnOff;
	IBOutlet UISwitch *switchVolume;
	IBOutlet UISwitch	 *switchSelective;
	IBOutlet UIButton *btnAdvanced;
	
	ThemeSongAppDelegate *appDelegate;
}

@property (nonatomic, retain) ThemeSongViewController *themeController;
@property (nonatomic, retain) AdvancedSettingViewController *advancedController;
@property (nonatomic, retain) ThemeSongAppDelegate *appDelegate;

-(IBAction)sliderValueChanged:(id)sender;
-(IBAction)switchPressed:(id)sender;
-(IBAction)btnPressed:(id)sender;

@end
