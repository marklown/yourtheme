/* ThemeSong
 * Copyright (C) 2010-2013  Mark Lown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#import <UIKit/UIKit.h>
@class ThemeSongAppDelegate;
@class ThemeSongViewController;

@interface SettingsViewController : UITableViewController {
	
	ThemeSongViewController *themeController;
	ThemeSongAppDelegate *appDelegate;
	IBOutlet UITableView *myTableView;
	
	/* Basic settings controls */
	UISlider *sliderMinVolume;
	UISlider *sliderMaxVolume;
	UISegmentedControl *modeControl;
	UISwitch *switchOppositeDay;
	
	/* Advanced settings controls */
	UISlider *sliderPlayLoopUpdateRate;
	UISlider *sliderAccLoopUpdateRate;
	UISlider *sliderDotProductThreshold;
	UISlider *sliderCountToChangeState;
	UISlider *sliderVolumeControlFactor;
	UILabel *lblPlayLoopUpdateRate;
	UILabel *lblAccLoopUpdateRate;
	UILabel *lblDotProductThreshold;
	UILabel *lblCountToChangeState;
	UILabel *lblVolumeControlFactor;
	
	/* Text */
	NSArray *txtArray;
}

@property (nonatomic, retain) ThemeSongViewController *themeController;
@property (nonatomic, retain) NSArray *txtArray;

-(void)sliderAction:(UISlider *)sender;
-(void)switchAction:(UISwitch *)sender;
-(void)buttonAction:(UISegmentedControl *)sender;

@end
