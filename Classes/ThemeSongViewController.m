/* ThemeSong
 * Copyright (C) 2010-2013  Mark Lown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#import "ThemeSongViewController.h"
#import "ThemeSongAppDelegate.h"
#import "BasicSettingViewController.h"
#import "SettingsViewController.h"

@implementation ThemeSongViewController
@synthesize player, picker, theme, accelerometer, basicSettingsViewController, settingsViewController;
@synthesize threshold, playLoopUpdateRate, accLoopUpdateRate, mode, cntToChangeState, volumeMin, volumeMax, volumeScaleFactor;
@synthesize defThreshold, defPlayLoopUpdateRate, defAccLoopUpdateRate, defMode, defCntToChangeState, defVolumeMin, defVolumeMax, defVolumeScaleFactor;
@synthesize oppositeDay, defOppositeDay;

/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

- (void)viewWillAppear:(BOOL)animated{
	self.title = @"urTheme";
	ThemeSongAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
	[appDelegate.rootController setNavigationBarHidden:NO animated:NO];
	self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] 
											  initWithTitle:@"Settings" 
											  style:UIBarButtonSystemItemAdd 
											  target:self
											   action:@selector(settingsPressed)] autorelease];
}

-(void)settingsPressed{
	settingsViewController = [[SettingsViewController alloc] initWithNibName:@"SettingsView" bundle:nil];
	ThemeSongAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.themeController = self;
	[appDelegate.rootController pushViewController:settingsViewController animated:YES];
	
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	// Get a handle to the app delegate
	ThemeSongAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.rootController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
	//appDelegate.rootController.navigationBar.tintColor = [UIColor blackColor];//[UIColor colorWithRed:(183.0/255.0) green:(244.0/255.0) blue:(110.0/255.0) alpha:.75];
	//[appDelegate.rootController setNavigationBarHidden:YES animated:NO];
	self.title = @"urTheme";
	
	// init locals
	shouldPlay = NO;
	first = YES;
	btnShowsPlay = YES;
	run = NO;
	on = 0;
	off = 0;
	cnt = 0;
	songSelected = NO;
	
	// Initialize default properties
	defCntToChangeState = 3;
	defVolumeMax = .5;
	defVolumeMin = .2;
	defMode = utModeVolume;
	defThreshold = .990;
	defPlayLoopUpdateRate = .10;
	defAccLoopUpdateRate = .175;
	defVolumeScaleFactor = 1;
	defOppositeDay = NO;
	
	// Init active properties to defaults
	cntToChangeState = defCntToChangeState;
	volumeMax = defVolumeMax;
	volumeMin = defVolumeMin;
	mode = defMode;
	threshold = defThreshold;
	playLoopUpdateRate = defPlayLoopUpdateRate;
	accLoopUpdateRate = defAccLoopUpdateRate;
	volumeScaleFactor = defVolumeScaleFactor;
	oppositeDay = defOppositeDay;
	
	// Setup the player
	player = [MPMusicPlayerController applicationMusicPlayer];
	player.volume = volumeMax;
	
	// Setup the accelerometer
	self.accelerometer = [UIAccelerometer sharedAccelerometer];
	self.accelerometer.updateInterval = accLoopUpdateRate;
	self.accelerometer.delegate = self;
}

/* Themez button pressed */
-(IBAction)btnThemezPressed:(id)sender{
	UIButton *theBtn = (UIButton *)sender;
	if(theBtn == btnThemez){
		picker = [[MPMediaPickerController alloc] initWithMediaTypes:MPMediaTypeAnyAudio];
		[[picker view] setFrame:CGRectMake(0,0,320,480)];
		picker.delegate = self;
		picker.allowsPickingMultipleItems = NO;
		picker.prompt = @"Select yo' theme";
		[self presentModalViewController:picker animated:YES];
	}else if(theBtn == btnPlay){
		if(btnShowsPlay){
			if(!songSelected){
				UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"Oops" message:@"You forgot to select a song." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] autorelease];
				[alert show];
				return;
			}
			firstVol = YES;
			run = YES;
			[player setQueueWithItemCollection:theme];
			[self performSelectorInBackground:@selector(playInBackground) withObject:nil];
			[theBtn setBackgroundImage:[UIImage imageNamed:@"urTheme_btnPauseMetal.png"] forState:UIControlStateNormal];
			btnShowsPlay = NO;
		}else{
			firstVol = NO;
			[player stop];
			[theBtn setBackgroundImage:[UIImage imageNamed:@"urTheme_btnPlayMetal.png"] forState:UIControlStateNormal];
			btnShowsPlay = YES;
			run = NO;
		}
	}
}

/* Handle changing the state of the player */
-(void)playInBackground{
	BOOL playing = NO;
	shouldPlay = NO;
	
	// *** Loop forever, need to change to to running flag!
	while(run){
		
		// Control differently depending on the mode we're in
		switch(mode){
				
			/* On Off mode */
			case utModeOnOff:
				if(shouldPlay && !playing){
					if(!oppositeDay){
						[player setVolume:volumeMax];
						[player play];
						playing = YES;
					}else{
						[player pause];
						playing = YES;
					}
				}else if(!shouldPlay && playing){
					if(!oppositeDay){
						[player pause];
						playing = NO;
					}else{
						[player setVolume:volumeMax];
						[player play];
						playing = NO;
					}
				}
				break;
				
			/* Volume control mode */
			case utModeVolume:
				if(firstVol){
					[player setVolume:volumeMin];
					[player play];
					firstVol = NO;
				}
				if(shouldPlay){
					if(!oppositeDay){
						if(player.volume < volumeMax){
							[player setVolume:player.volume + playLoopUpdateRate*volumeScaleFactor];
						}
					}else{
						if(player.volume > volumeMin){
							[player setVolume:player.volume - playLoopUpdateRate*volumeScaleFactor];
						}
					}
				}else if(!shouldPlay){
					if(!oppositeDay){
						if(player.volume > volumeMin){
							[player setVolume:player.volume - playLoopUpdateRate*volumeScaleFactor];
						}
					}else{
						if(player.volume < volumeMax){
							[player setVolume:player.volume + playLoopUpdateRate*volumeScaleFactor];
						}
					}
				}
				break;
			
			/* Selective song mode */
			case utModeSelective:
				break;
		}
		
		// Be good
		[NSThread sleepForTimeInterval:playLoopUpdateRate];
	}
}

/* MediaPicker delegate method:
 * Called when the user has selected a set of media items */
-(void)mediaPicker:(MPMediaPickerController *)thePicker didPickMediaItems:(MPMediaItemCollection *)theThemez{
	theme = [[MPMediaItemCollection alloc] initWithItems:[theThemez items]];
	[thePicker dismissModalViewControllerAnimated:YES];
	songSelected = YES;
}

/* MediaPicker delegate method:
 * User cancelled */
-(void)mediaPickerDidCancel:(MPMediaPickerController *)thePicker{
	[thePicker dismissModalViewControllerAnimated:YES];
	songSelected = NO;
}

/* Accelerometer delegate method:
 * Handle data received from the accelerometer */
-(void)accelerometer:(UIAccelerometer *)theAccelerometer didAccelerate:(UIAcceleration *)theAcceleration{

	// Increment the counter
	cnt++;
	
	xx = theAcceleration.x;
	yy = theAcceleration.y;
	zz = theAcceleration.z;

	// Get the difference between the last and current force vectors
	float dot = (px * xx) + (py * yy) + (pz * zz);
	float a = ABS(sqrt(px * px + py * py + pz * pz));
	float b = ABS(sqrt(xx * xx + yy * yy + zz * zz));
	dot /= (a * b);
	
	// Compare it to the threshold
	if(dot < threshold){
		on++;
	}else{
		off++;
	}
	
	// Determine if we change state
	if(cnt > cntToChangeState){
		if(on > off){
			shouldPlay = YES;
		}else{
			shouldPlay = NO;
		}
		cnt = 0;
		on = 0;
		off = 0;
	}
	
	// Set the last values to current
	px = xx;
	py = yy;
	pz = zz;
	
	//NSLog(@"on:@%d  off:@%d cnt @%d",on,off,cnt);
	first = NO;
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[settingsViewController release];
	[basicSettingsViewController release];
	[accelerometer release];
	[theme release];
	[player release];
	[picker release];
    [super dealloc];
}

@end
