//
//  AdvancedSettingViewController.m
//  ThemeSong
//
//  Created by Mark Lown on 9/23/10.
//  Copyright 2010 PopCulture Development, LLC. All rights reserved.
//

#import "AdvancedSettingViewController.h"
#import "ThemeSongAppDelegate.h"
#import "ThemeSongViewController.h"

@implementation AdvancedSettingViewController
@synthesize themeController, appDelegate;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.appDelegate = [[UIApplication sharedApplication] delegate];
	self.themeController = appDelegate.themeController;
	
	txtPlayLoopUpdateRate.text = [NSString stringWithFormat:@"%.3f", self.themeController.playLoopUpdateRate];
	txtAccLoopUpdateRate.text = [NSString stringWithFormat:@"%.3f", self.themeController.accLoopUpdateRate];
	txtThreshold.text = [NSString stringWithFormat:@"%.3f", self.themeController.threshold];
	txtCount.text = [NSString stringWithFormat:@"%d", self.themeController.cntToChangeState];
}


-(void)viewWillAppear:(BOOL)animated{
	NSLog(@"%f",self.themeController.playLoopUpdateRate);

}

-(IBAction)txtUpdated:(id)sender{
	UITextField *txt = (UITextField *)sender;
	if(txt == txtPlayLoopUpdateRate){
		themeController.playLoopUpdateRate = [txt.text doubleValue];
	}else if(txt == txtAccLoopUpdateRate){
		themeController.accLoopUpdateRate = [txt.text doubleValue];
	}else if(txt == txtThreshold){
		themeController.threshold = [txt.text doubleValue];
	}else if(txt == txtCount){
		themeController.cntToChangeState = [txt.text intValue];
	}
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
	[textField resignFirstResponder];
	return YES;
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[appDelegate release];
	[themeController release];
    [super dealloc];
}


@end
